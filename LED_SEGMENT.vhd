library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

entity LED_SEGMENT is
  port (
    i_clock	: in std_logic;
	i_EN	: in std_logic;
	i_BCD1	: in std_logic_vector(15 downto 0);
	i_BCD2	: in std_logic_vector(15 downto 0);
	o_sgmLEDs : out std_logic_vector(7 downto 0);
	o_sgmMUX  : out std_logic_vector(2 downto 0)
	
  ) ;
end LED_SEGMENT ; 

architecture arch of LED_SEGMENT is
	type BCD_lookup is array (0 to 9) of std_logic_vector(3 downto 0);
	type segments_lut is array (0 to 15) of std_logic_vector(7 downto 0);
	type segments_reg is array (0 to 7 ) of std_logic_vector(7 downto 0);	
	--signals
	signal leds : std_logic_vector(7 downto 0) := "11000000";
	signal mux  : std_logic_vector(2 downto 0) := "000";
	signal BCD	: BCD_lookup :=("0000","0001","0010","0011","0100",
								"0101","0110","0111","1000","1001");

	signal segments_lookup : segments_lut := (X"C0",X"F9",X"A4",X"B0",X"99",
											X"92",X"82",X"F8",X"80",X"90",
											X"88",X"83",X"C6",X"A1",X"86",X"8E");

	signal segments   : segments_reg := (segments_lookup(0),segments_lookup(3),
								  segments_lookup(2),segments_lookup(5),
								  segments_lookup(4),segments_lookup(8),
								  segments_lookup(0),segments_lookup(0));

	signal BCD_digit0 : std_logic_vector(3 downto 0) := "0000";
	signal BCD_digit1 : std_logic_vector(3 downto 0) := "0000";
	signal BCD_digit2 : std_logic_vector(3 downto 0) := "0000";
	signal BCD_digit3 : std_logic_vector(3 downto 0) := "0000";
	signal BCD_digit4 : std_logic_vector(3 downto 0) := "0000";
	signal BCD_digit5 : std_logic_vector(3 downto 0) := "0000";
	signal BCD_digit6 : std_logic_vector(3 downto 0) := "0000";
	signal BCD_digit7 : std_logic_vector(3 downto 0) := "0000";

begin
	  --Multiplex LED Segment
	p_MUX : process( i_clock )
	begin
		if rising_edge(i_clock) then
			for i in 0 to 7 loop
					mux <= std_logic_vector(unsigned(mux)-i);
				end loop ; 
		end if ;
	end process ; -- p_Matrix

	p_setValues : process( i_clock )
    variable leds : natural range 0 to 8 :=0;
    begin
      if rising_edge(i_clock) then
        if leds = 8 then
          leds:=0;
        end if ;
        leds := leds+1;
        o_sgmLEDs<= segments(leds);        
      end if ;
  	end process ; --  p_setValues  

	bcde1r : process( i_BCD1 )
	begin
		--the display is split in two, each i_BCD controls one half
		--digit 0 converter
		case( BCD_digit0 ) is
			when BCD(0) => segments(4) <=segments_lookup(0);
			when BCD(1)	=> segments(4) <=segments_lookup(1);	
			when BCD(2) => segments(4) <=segments_lookup(2);
			when BCD(3)	=> segments(4) <=segments_lookup(3);	
			when BCD(4) => segments(4) <=segments_lookup(4);
			when BCD(5)	=> segments(4) <=segments_lookup(5);	
			when BCD(6) => segments(4) <=segments_lookup(6);
			when BCD(7)	=> segments(4) <=segments_lookup(7);
			when BCD(8) => segments(4) <=segments_lookup(8);
			when BCD(9) => segments(4) <=segments_lookup(9);		
			when others => report "undefined" severity failure;
		end case ;

		case( BCD_digit1 ) is
			when BCD(0) => segments(5) <=segments_lookup(0);
			when BCD(1)	=> segments(5) <=segments_lookup(1);	
			when BCD(2) => segments(5) <=segments_lookup(2);
			when BCD(3)	=> segments(5) <=segments_lookup(3);	
			when BCD(4) => segments(5) <=segments_lookup(4);
			when BCD(5)	=> segments(5) <=segments_lookup(5);	
			when BCD(6) => segments(5) <=segments_lookup(6);
			when BCD(7)	=> segments(5) <=segments_lookup(7);
			when BCD(8) => segments(5) <=segments_lookup(8);
			when BCD(9) => segments(5) <=segments_lookup(9);		
			when others => report "undefined" severity failure;
		end case ;
		case( BCD_digit2 ) is
			when BCD(0) => segments(6) <=segments_lookup(0);
			when BCD(1)	=> segments(6) <=segments_lookup(1);	
			when BCD(2) => segments(6) <=segments_lookup(2);
			when BCD(3)	=> segments(6) <=segments_lookup(3);	
			when BCD(4) => segments(6) <=segments_lookup(4);
			when BCD(5)	=> segments(6) <=segments_lookup(5);	
			when BCD(6) => segments(6) <=segments_lookup(6);
			when BCD(7)	=> segments(6) <=segments_lookup(7);
			when BCD(8) => segments(6) <=segments_lookup(8);
			when BCD(9) => segments(6) <=segments_lookup(9);		
			when others => report "undefined" severity failure;
		end case ;
		case( BCD_digit3 ) is
			when BCD(0) => segments(7) <=segments_lookup(0);
			when BCD(1)	=> segments(7) <=segments_lookup(1);	
			when BCD(2) => segments(7) <=segments_lookup(2);
			when BCD(3)	=> segments(7) <=segments_lookup(3);	
			when BCD(4) => segments(7) <=segments_lookup(4);
			when BCD(5)	=> segments(7) <=segments_lookup(5);	
			when BCD(6) => segments(7) <=segments_lookup(6);
			when BCD(7)	=> segments(7) <=segments_lookup(7);
			when BCD(8) => segments(7) <=segments_lookup(8);
			when BCD(9) => segments(7) <=segments_lookup(9);		
			when others => report "undefined" severity failure;
		end case ;
	end process ; -- bcder1

	bcde2r : process( i_BCD2 )
	begin
		--the display is split in two, each i_BCD controls one half
		--digit 0 converter
		case( BCD_digit4 ) is
			when BCD(0) => segments(0) <=segments_lookup(0);
			when BCD(1)	=> segments(0) <=segments_lookup(1);	
			when BCD(2) => segments(0) <=segments_lookup(2);
			when BCD(3)	=> segments(0) <=segments_lookup(3);	
			when BCD(4) => segments(0) <=segments_lookup(4);
			when BCD(5)	=> segments(0) <=segments_lookup(5);	
			when BCD(6) => segments(0) <=segments_lookup(6);
			when BCD(7)	=> segments(0) <=segments_lookup(7);
			when BCD(8) => segments(0) <=segments_lookup(8);
			when BCD(9) => segments(0) <=segments_lookup(9);		
			when others => 
		end case ;

		case( BCD_digit5 ) is
			when BCD(0) => segments(1) <=segments_lookup(0);
			when BCD(1)	=> segments(1) <=segments_lookup(1);	
			when BCD(2) => segments(1) <=segments_lookup(2);
			when BCD(3)	=> segments(1) <=segments_lookup(3);	
			when BCD(4) => segments(1) <=segments_lookup(4);
			when BCD(5)	=> segments(1) <=segments_lookup(5);	
			when BCD(6) => segments(1) <=segments_lookup(6);
			when BCD(7)	=> segments(1) <=segments_lookup(7);
			when BCD(8) => segments(1) <=segments_lookup(8);
			when BCD(9) => segments(1) <=segments_lookup(9);		
			when others =>
		end case ;
		case( BCD_digit6 ) is
			when BCD(0) => segments(2) <=segments_lookup(0);
			when BCD(1)	=> segments(2) <=segments_lookup(1);	
			when BCD(2) => segments(2) <=segments_lookup(2);
			when BCD(3)	=> segments(2) <=segments_lookup(3);	
			when BCD(4) => segments(2) <=segments_lookup(4);
			when BCD(5)	=> segments(2) <=segments_lookup(5);	
			when BCD(6) => segments(2) <=segments_lookup(6);
			when BCD(7)	=> segments(2) <=segments_lookup(7);
			when BCD(8) => segments(2) <=segments_lookup(8);
			when BCD(9) => segments(2) <=segments_lookup(9);		
			when others =>
		end case ;
		case( BCD_digit7 ) is
			when BCD(0) => segments(3) <=segments_lookup(0);
			when BCD(1)	=> segments(3) <=segments_lookup(1);	
			when BCD(2) => segments(3) <=segments_lookup(2);
			when BCD(3)	=> segments(3) <=segments_lookup(3);	
			when BCD(4) => segments(3) <=segments_lookup(4);
			when BCD(5)	=> segments(3) <=segments_lookup(5);	
			when BCD(6) => segments(3) <=segments_lookup(6);
			when BCD(7)	=> segments(3) <=segments_lookup(7);
			when BCD(8) => segments(3) <=segments_lookup(8);
			when BCD(9) => segments(3) <=segments_lookup(9);		
			when others =>
		end case ;
	end process ; -- bcder2

	--o_sgmLEDs<=leds;
	BCD_digit0 <= i_BCD1(3 downto 0);
	BCD_digit1 <= i_BCD1(7 downto 4);
	BCD_digit2 <= i_BCD1(11 downto 8);
	BCD_digit3 <= i_BCD1(15 downto 12);

	BCD_digit4 <= i_BCD2(3 downto 0);
	BCD_digit5 <= i_BCD2(7 downto 4);
	BCD_digit6 <= i_BCD2(11 downto 8);
	BCD_digit7 <= i_BCD2(15 downto 12);
	o_sgmMUX<=mux;
end architecture ;