library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

entity BTN_DEBOUNCE is
  port (
    i_clock : in std_logic;
    i_buttons: in std_logic_vector(3 downto 0);
    o_buttons: out std_logic_vector(3 downto 0)
  ) ;
end BTN_DEBOUNCE ; 

architecture arch of BTN_DEBOUNCE is
  constant div_constant: natural:=500000;

  signal dividedClock  : std_logic;
  signal cnt  : natural range 0 to div_constant := 0;
  signal tempStore: std_logic_vector(3 downto 0);
begin
  process( i_clock )
  begin
    if rising_Edge(i_clock) then
      cnt <= cnt+1;
      if (cnt=div_constant-1) then
        dividedClock <= NOT dividedClock;
        cnt <= 0;
      end if ;
    end if ;
  end process ;   
  
  debouncer : process( dividedClock )
  variable TMP:std_logic_vector(3 downto 0):="0000";
  begin
    if rising_Edge(dividedClock) then
      TMP := i_buttons;
    end if;
    if falling_Edge(dividedClock) then
      if TMP=i_buttons then
        tempStore<=TMP;
      end if ;
    end if;
  end process ; -- debouncer
    o_buttons<=tempStore;
end architecture ;