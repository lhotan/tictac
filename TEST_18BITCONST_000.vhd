library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

entity TEST_18BITCONST is
    generic(
    const1 : std_logic_vector(17 downto 0):="000000000000000000"
    );
  port (
    o_const : out std_logic_vector(17 downto 0)
  ) ;
end TEST_18BITCONST ; 

architecture arch of TEST_18BITCONST is

begin
    o_const<=const1;
end architecture ;