library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

entity TONE is
  port (
    --50MHz clock in!!
    i_clock : in std_logic;
    o_toneOut : out std_logic;
    i_toggleTone : in std_logic;
    i_toneSelect : in std_logic_vector(2 downto 0)
  ) ;
end TONE ; 

architecture arch of TONE is
   
    signal tone12000Hz : std_logic_vector(12 downto 0);
    signal tone6000Hz  : std_logic_vector(13 downto 0);
    signal tone3000Hz  : std_logic_vector(14 downto 0);
    signal tone1500Hz  : std_logic_vector(15 downto 0);
    signal tone762Hz   : std_logic_vector(16 downto 0);
    signal tone381Hz   : std_logic_vector(17 downto 0);
    signal tone190Hz   : std_logic_vector(18 downto 0);
    signal tone95Hz    : std_logic_vector(19 downto 0);
    signal tone47Hz    : std_logic_vector(20 downto 0);

    signal toggle : std_logic:='1';
    signal selectTone : std_logic_vector(2 downto 0);

    signal toggle1 : std_logic:=tone12000Hz(12);
    signal toggle2 : std_logic:=tone6000Hz(13);
    signal toggle3 : std_logic:=tone3000Hz(14);
    signal toggle4 : std_logic:=tone1500Hz(15);
    signal toggle5 : std_logic:=tone762Hz(16);
    signal toggle6 : std_logic:=tone381Hz(17);
    signal toggle7 : std_logic:=tone190Hz(18);
    signal toggle8 : std_logic:=tone95Hz(19);
   -- signal toggle9 : std_logic:=tone47Hz(20);
begin


    tonegen : process( i_clock )
    begin
      if rising_Edge(i_clock) then
        tone12000Hz <= std_logic_vector(unsigned(tone12000Hz)+1);  
        tone6000Hz <= std_logic_vector(unsigned(tone6000Hz)+1); 
        tone3000Hz <= std_logic_vector(unsigned(tone3000Hz)+1); 
        tone1500Hz <= std_logic_vector(unsigned(tone1500Hz)+1); 
        tone762Hz <= std_logic_vector(unsigned(tone762Hz)+1); 
        tone381Hz <= std_logic_vector(unsigned(tone381Hz)+1); 
        tone190Hz <= std_logic_vector(unsigned(tone190Hz)+1); 
        tone95Hz <= std_logic_vector(unsigned(tone95Hz)+1); 
        tone47Hz <= std_logic_vector(unsigned(tone47Hz)+1);           
      end if ;
    end process ;--tonegen
    
    toggle1 <=tone12000Hz(12);
    toggle2 <=tone6000Hz(13);
    toggle3 <=tone3000Hz(14);
    toggle4 <=tone1500Hz(15);
    toggle5 <=tone762Hz(16);
    toggle6 <=tone381Hz(17);
    toggle7 <=tone190Hz(18);
    toggle8 <=tone95Hz(19);

    selectTone<=i_toneSelect;
   -- toggle <= i_toggleTone;
    o_toneOut <= toggle1 when toggle='1' AND selectTone="000" else
        toggle2 when toggle='1' AND selectTone="001" else
        toggle3 when toggle='1' AND selectTone="011" else
        toggle4 when toggle='1' AND selectTone="100" else
        toggle5 when toggle='1' AND selectTone="101" else
        toggle6 when toggle='1' AND selectTone="110" else
        toggle7 when toggle='1' AND selectTone="111"; 
end architecture ;