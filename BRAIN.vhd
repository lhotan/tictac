library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

entity BRAIN is
  port (
    i_clock   : in std_logic;
    i_buttons : in std_logic_vector(3 downto 0);
    o_toneSelect : out std_logic_vector(2 downto 0);
    o_toneEN    : out std_logic;
    o_grid      : out std_logic_vector(17 downto 0);
    o_testLeds  : out std_logic_vector(3 downto 0);
    o_reset: out std_logic;
    o_segment0: out std_logic_vector(15 downto 0);
    o_segment1: out std_logic_vector(15 downto 0)
  ) ;
end BRAIN ; 

architecture arch of BRAIN is
    type BCD_lookup is array (0 to 9) of std_logic_vector(3 downto 0);
    type FSM_types is(GAME_RUN,GAME_IDLE,GAME_RESET,GAME_COUNT_POINTS);

    constant BCD	: BCD_lookup :=("0000","0001","0010","0011","0100",
    "0101","0110","0111","1000","1001");

    signal player0dots : std_logic_vector(8 downto 0):="000000000";
    signal player1dots : std_logic_vector(8 downto 0):="000000000";

    signal currentPlayer: std_logic:='0';
    signal grid : std_logic_vector(17 downto 0):="000000000000000000";
    signal grid2 : std_logic_vector(17 downto 0):="000000000000000000";
	signal mbuttons: std_logic_vector(3 downto 0):="1111";
    signal game_state : std_logic_vector(3 downto 0):="1111";
    
    constant delay_cycles : natural:=50000000;
    signal delay_tmp  : std_logic;
    signal delay_cnt  : natural range 0 to delay_cycles;
    signal reset: std_logic:='0';
    signal counted : std_logic:='0';

    signal segment0 : std_logic_vector(15 downto 0);
    signal segment1 : std_logic_vector(15 downto 0);

    signal player0points: std_logic_vector(1 downto 0):="00";
    signal player1points: std_logic_vector(1 downto 0):="00";

    signal STATE: FSM_types:=GAME_RESET;
begin

    delayer : process( i_clock, reset )
    begin
        if reset='1' then
            if rising_Edge(i_clock) then
              delay_cnt<=delay_cnt+1;
              if delay_cnt>=delay_cycles then
                  counted<='1';
              end if ;
            end if;
        else 
            delay_cnt<=0;
            counted<='0';
        end if ;
    end process ; -- delayer

    --TODO: Implement FSM
    --assigning values from keypad to grid
    buttons : process( i_clock )
    variable moves : natural range 0 to 10;
    begin
        if rising_Edge(i_clock) then       
            --signal can be assigned only in one process
            case( STATE ) is
                when GAME_IDLE=>
                 STATE<=GAME_RUN;
                when GAME_RUN =>
                case( game_state ) is
                    --prevent lock up when game is a draw
                    --game in progress
                    when "1111" =>
                    if moves>=9 then
                        STATE<=GAME_RESET;
                    end if ;
                    case( mbuttons ) is
                        ------
                        when BCD(1) =>
                            case( grid(17 downto 16) ) is
                                when "00" =>
                                    case( currentPlayer ) is
                                        when '0' =>
                                            grid(17 downto 16) <="01";  
                                            currentPlayer<='1';
                                            moves:=moves+1;
                                        when '1' =>
                                            grid(17 downto 16) <= "10";
                                            currentPlayer<='0';
                                            moves:=moves+1;
                                        when others =>
                                    end case ;
                                when others =>
                                --error, already selected, beep or something       
                            end case ;
                        -------
                        when BCD(2) =>
                            case( grid(15 downto 14) ) is
                                when "00" =>
                                    case( currentPlayer ) is
                                        when '0' =>
                                            grid(15 downto 14) <="01";  
                                            currentPlayer<='1';
                                            moves:=moves+1;
                                        when '1' =>
                                            grid(15 downto 14) <= "10";
                                            currentPlayer<='0';
                                            moves:=moves+1;
                                        when others =>
                                    end case ;
                                when others =>
                                --error, already selected, beep or something       
                            end case ;
                        --------
                        when BCD(3) =>
                            case( grid(13 downto 12) ) is
                                when "00" =>
                                    case( currentPlayer ) is
                                        when '0' =>
                                            grid(13 downto 12) <="01";  
                                            currentPlayer<='1';
                                            moves:=moves+1;
                                        when '1' =>
                                            grid(13 downto 12) <= "10";
                                            currentPlayer<='0';
                                            moves:=moves+1;
                                        when others =>
                                    end case ;
                                when others =>
                                --error, already selected, beep or something 
                            end case; 
                        ----------
                        when BCD(4) =>
                        case( grid(11 downto 10) ) is
                            when "00" =>
                                case( currentPlayer ) is
                                    when '0' =>
                                        grid(11 downto 10) <="01";  
                                        currentPlayer<='1';
                                        moves:=moves+1;
                                    when '1' =>
                                        grid(11 downto 10) <= "10";
                                        currentPlayer<='0';
                                        moves:=moves+1;
                                    when others =>
                                end case ;
                            when others =>
                            --error, already selected, beep or something 
                        end case; 
                        --------
                        when BCD(5) => 
                        case( grid(9 downto 8) ) is
                            when "00" =>
                                case( currentPlayer ) is
                                    when '0' =>
                                        grid(9 downto 8) <="01";  
                                        currentPlayer<='1';
                                        moves:=moves+1;
                                    when '1' =>
                                        grid(9 downto 8) <= "10";
                                        currentPlayer<='0';
                                        moves:=moves+1;
                                    when others =>
                                end case ;
                            when others =>
                            --error, already selected, beep or something 
                        end case; 
                        --------
                        when BCD(6) =>
                        case( grid(7 downto 6) ) is
                            when "00" =>
                                case( currentPlayer ) is
                                    when '0' =>
                                        grid(7 downto 6) <="01";  
                                        currentPlayer<='1';
                                        moves:=moves+1;
                                    when '1' =>
                                        grid(7 downto 6) <= "10";
                                        currentPlayer<='0';
                                        moves:=moves+1;
                                    when others =>
                                end case ;
                            when others =>
                            --error, already selected, beep or something 
                        end case; 
                        -------
                        when BCD(7) =>
                        case( grid(5 downto 4) ) is
                            when "00" =>
                                case( currentPlayer ) is
                                    when '0' =>
                                        grid(5 downto 4) <="01";  
                                        currentPlayer<='1';
                                        moves:=moves+1;
                                    when '1' =>
                                        grid(5 downto 4) <= "10";
                                        currentPlayer<='0';
                                        moves:=moves+1;
                                    when others =>
                                end case ;
                            when others =>
                            --error, already selected, beep or something 
                        end case; 
                        ----------
                        when BCD(8) =>
                        case( grid(3 downto 2) ) is
                            when "00" =>
                                case( currentPlayer ) is
                                    when '0' =>
                                        grid(3 downto 2) <="01";  
                                        currentPlayer<='1';
                                        moves:=moves+1;
                                    when '1' =>
                                        grid(3 downto 2) <= "10";
                                        currentPlayer<='0';
                                        moves:=moves+1;
                                    when others =>
                                end case ;
                            when others =>
                            --error, already selected, beep or something 
                        end case; 
                        ----------
                        when BCD(9) =>
                        case( grid(1 downto 0) ) is
                            when "00" =>
                                case( currentPlayer ) is
                                    when '0' =>
                                        grid(1 downto 0) <="01";  
                                        currentPlayer<='1';
                                        moves:=moves+1;
                                    when '1' =>
                                        grid(1 downto 0) <= "10";
                                        currentPlayer<='0';
                                        moves:=moves+1;
                                    when others =>
                                end case ;
                            when others =>
                            --error, already selected, beep or something 
                        end case; 
                        when others =>
                    
                    end case ;
                    --game end/reset/increment counter
                    when others =>
                        STATE<=GAME_COUNT_POINTS;
                    end case ;   

                when GAME_COUNT_POINTS=>
                    case( game_state ) is
                        when "1010" =>
                        player0points<=std_logic_vector(unsigned(player0points)+1);
                        when "0101" =>
                        player1points<=std_logic_vector(unsigned(player1points)+1);
                        when others =>
                    
                    end case ;
                STATE<=GAME_RESET;
                when GAME_RESET=>
                --keypad finished restarting
                if (i_buttons="1111" AND counted='1') then
                    reset<='0';
                    o_reset<='0';
                    STATE<=GAME_IDLE;
                    else
                    --TODO: display which player won here
                        if  player0points="11" OR player1points="11" then
                          player0points<="00";
                          player1points<="00";
                        end if ;
                        moves:=0;
                        o_reset<='1';--reset keypad
                        reset<='1'; --start counter
                        grid<="000000000000000000";
                        currentPlayer<='0';
                    end if ;
                when others =>
            
            end case ;
           
       end if ;
    end process ; -- mbuttons
    

    segment0(1 downto 0)<=player0points;
    segment1(1 downto 0)<=player1points;

    player0dots(8) <= '1' when grid(17 downto 16)="01" else '0';
    player0dots(7) <= '1' when grid(15 downto 14)="01" else '0';
    player0dots(6) <= '1' when grid(13 downto 12)="01" else '0';
    player0dots(5) <= '1' when grid(11 downto 10)="01" else '0';
    player0dots(4) <= '1' when grid(9 downto 8)="01" else '0';
    player0dots(3) <= '1' when grid(7 downto 6)="01" else '0';
    player0dots(2) <= '1' when grid(5 downto 4)="01" else '0';
    player0dots(1) <= '1' when grid(3 downto 2)="01" else '0';
    player0dots(0) <= '1' when grid(1 downto 0)="01" else '0';

	player1dots(8) <= '1' when grid(17 downto 16)="10" else '0';
    player1dots(7) <= '1' when grid(15 downto 14)="10" else '0';
    player1dots(6) <= '1' when grid(13 downto 12)="10" else '0';
    player1dots(5) <= '1' when grid(11 downto 10)="10" else '0';
    player1dots(4) <= '1' when grid(9 downto 8)="10" else '0';
    player1dots(3) <= '1' when grid(7 downto 6)="10" else '0';
    player1dots(2) <= '1' when grid(5 downto 4)="10" else '0';
    player1dots(1) <= '1' when grid(3 downto 2)="10" else '0';
    player1dots(0) <= '1' when grid(1 downto 0)="10" else '0';
                    
    game_state<=    --player0
                    "1010" when player0dots(8 downto 6)="111" else
					"1010" when player0dots(5 downto 3)="111" else
					"1010" when player0dots(2 downto 0)="111" else
					--vertical
					"1010" when player0dots(8)='1' AND player0dots(5)='1' AND player0dots(2)='1' else
					"1010" when player0dots(7)='1' AND player0dots(4)='1' AND player0dots(1)='1' else
					"1010" when player0dots(6)='1' AND player0dots(3)='1' AND player0dots(0)='1' else
					--horizontal
					"1010" when player0dots(8)='1' AND player0dots(4)='1' AND player0dots(0)='1' else
					"1010" when player0dots(6)='1' AND player0dots(4)='1' AND player0dots(2)='1' else		 
					
					--player1
					"0101" when player1dots(8 downto 6)="111" else
					"0101" when player1dots(5 downto 3)="111" else
					"0101" when player1dots(2 downto 0)="111" else
					--vertical
					"0101" when player1dots(8)='1' AND player1dots(5)='1' AND player1dots(2)='1' else
					"0101" when player1dots(7)='1' AND player1dots(4)='1' AND player1dots(1)='1' else
					"0101" when player1dots(6)='1' AND player1dots(3)='1' AND player1dots(0)='1' else
					--horizontal
					"0101" when player1dots(8)='1' AND player1dots(4)='1' AND player1dots(0)='1' else
					"0101" when player1dots(6)='1' AND player1dots(4)='1' AND player1dots(2)='1' 
					else"1111";
			
    
    o_segment0<=segment0;
    o_segment1<=segment1;
	o_testLeds<=i_buttons;
    mbuttons<=i_buttons;
    o_grid <= grid;
end architecture ;