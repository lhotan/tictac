library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

entity MY_MUX2 is
  port (
    i_clock : in std_logic;
    i_mux     : in std_logic;
    i_matLEDs : in std_logic_vector(7 downto 0);
    i_matMUX  : in std_logic_vector(2 downto 0);
    i_sgmLEDs : in std_logic_vector(7 downto 0);
    i_sgmMUX  : in std_logic_vector(2 downto 0);
    o_LEDs    : out std_logic_vector(7 downto 0);
    o_ledMUX  : out std_logic_vector(2 downto 0);
    o_LEDsEN   : out std_logic_vector(1 downto 0)
  ) ;
end MY_MUX2 ; 

architecture arch of MY_MUX2 is
  signal mux : std_logic := '0';
begin
  mux<=i_mux;
  o_LEDs <= i_matLEDs when mux='1' else i_sgmLEDs;
  o_ledMUX <= i_matMUX when mux='1' else i_sgmMUX;
  o_LEDsEN <= "10" when mux='1' else "01";
end architecture ;