library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

entity LED_MATRIX is
  port (
    i_clock : in std_logic;
	i_en    : in std_logic;
	i_framesIn	: in std_logic_vector(7 downto 0);
	o_frameSelect : out std_logic_vector(2 downto 0);
    o_matLEDs : out std_logic_vector(7 downto 0);
    o_matMUX  : out std_logic_vector(2 downto 0)
  ) ;
end LED_MATRIX ; 

architecture arch of LED_MATRIX is
  signal leds : std_logic_vector(7 downto 0) := "10101010";
  signal mux  : std_logic_vector(2 downto 0) := "000";
  signal frameSelect  : std_logic_vector(2 downto 0) := "000";
begin

  --Multiplex LED Matrix
  p_MUX : process( i_clock )
    begin
        if rising_edge(i_clock) then
            for i in 0 to 7 loop
					mux <= std_logic_vector(unsigned(mux)-i);
                end loop ; 
        end if ;
	end process ; -- p_Matrix
	
  o_frameSelect <= mux;
  o_matMUX <= mux;
  o_matLEDs <= NOT i_framesIn;
end architecture ;