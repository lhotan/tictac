library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

entity TEST_2CONSTANTS16 is
  generic(
    const1 : std_logic_vector(15 downto 0):="0000000000000000";
    const2 : std_logic_vector(15 downto 0):="0000000000000000"
    );
  port (
    o_const1 : out std_logic_vector(15 downto 0);
    o_const2 : out std_logic_vector(15 downto 0)
  ) ;
end TEST_2CONSTANTS16 ; 

architecture arch of TEST_2CONSTANTS16 is
begin
    o_const1 <=const1;
    o_const2 <=const2;
end architecture ;