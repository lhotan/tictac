library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

entity DIVIDER is
  generic(div_constant : natural:=25000000);
  port (
    i_clock        : in std_logic;
    o_dividedClock : out std_logic
  ) ;
end DIVIDER ; 

architecture arch of DIVIDER is
  
  signal tmp  : std_logic;
  signal cnt  : natural range 0 to div_constant := 0;

begin
  process( i_clock )
  begin
    if rising_Edge(i_clock) then
      cnt <= cnt+1;
      if (cnt=div_constant-1) then
        tmp <= NOT tmp;
        cnt <= 0;
      end if ;
    end if ;
  end process ;   
  o_dividedClock <= tmp;  
end architecture ;
