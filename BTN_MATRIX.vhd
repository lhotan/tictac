library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;
    
entity BTN_MATRIX is
    port (
        i_clock : in std_logic;
        i_reset: in std_logic;
        i_rows : in std_logic_vector(2 downto 0);
        o_cols: out std_logic_vector(2 downto 0);
        o_data : out std_logic_vector(3 downto 0)
    );
end BTN_MATRIX;

architecture arch of BTN_MATRIX is
    type BCD_lookup is array (0 to 9) of std_logic_vector(3 downto 0);
    type MultiplexStates is array (0 to 2) of std_logic_vector(2 downto 0);

    signal dataOut  : std_logic_vector(3 downto 0):="1111";
    signal multiplex : std_logic_vector(2 downto 0) := "000";
    signal BCD	: BCD_lookup :=("0000","0001","0010","0011","0100",
                                "0101","0110","0111","1000","1001");
    signal multiState : MultiplexStates := ("110","101","011");
    signal outdat : std_logic_vector(3 downto 0) := "1111";
begin
    p_mult : process( i_clock, i_reset )
    variable btns : natural range 0 to 3 :=0;
    begin
        if i_reset='1' then
            dataOut<="1111";
        elsif rising_edge(i_clock) then
            multiplex<= multiState(btns);     
            btns := btns+1;
            case(multiplex) is
                when multiState(0)=>
                case( i_rows ) is
                    when "110" => dataOut <= BCD(1); 
                    when "101" => dataOut <= BCD(4);
                    when "011" => dataOut <= BCD(7);      
                    when others => 
                end case ;

                when multiState(1)=>
                case( i_rows ) is
                    when "110" => dataOut <= BCD(2); 
                    when "101" => dataOut <= BCD(5);
                    when "011" => dataOut <= BCD(8);      
                    when others =>
                end case ;

                when multiState(2)=>
                case( i_rows ) is
                    when "110" => dataOut <= BCD(3); 
                    when "101" => dataOut <= BCD(6);
                    when "011" => dataOut <= BCD(9);      
                    when others =>
                end case ;
                when others =>
            
            end case ;
        end if ;
      end process ; --  p_setValues  
    outdat <= dataOut;
    o_data <= outdat;
    o_cols <= multiplex;
end architecture ;