library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

entity DELAYER is
  generic(
       cycles : natural:=50000000
  );
  port (
    i_clock : in std_logic;
    i_en : in std_logic;
    o_counted: out std_logic
  ) ;
end DELAYER ; 

architecture arch of DELAYER is
    signal tmp  : std_logic;
    signal cnt  : natural range 0 to cycles := 0;  
begin
    counter : process( i_clock, i_en )
    variable enabled : std_logic;
    begin
        if rising_edge(i_clock) then
            cnt <= cnt+1;
            if cnt=cycles then
                tmp<=NOT tmp;
                cnt<=0;
            elsif i_en='0' then
                cnt<=0;
            end if;
        end if ;                
    end process ; -- counter

    o_counted <= tmp when i_en='1';
end architecture ;