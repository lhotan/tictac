library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

entity MAT_FRAMEBUFFER is

    generic(
    line0 : std_logic_vector(7 downto 0):="00000000";
    line1 : std_logic_vector(7 downto 0):="00000000";
    line2 : std_logic_vector(7 downto 0):="00000000";
    line3 : std_logic_vector(7 downto 0):="00000000";
    line4 : std_logic_vector(7 downto 0):="00000000";
    line5 : std_logic_vector(7 downto 0):="00000000";
    line6 : std_logic_vector(7 downto 0):="00000000";
    line7 : std_logic_vector(7 downto 0):="00000000"
    );

  port (
    i_clock : in std_logic;
    i_gridDat: in std_logic_vector(17 downto 0);
    i_selectLine : in std_logic_vector(2 downto 0):="000";
    o_frameData : out std_logic_vector(7 downto 0):="00000000"
  );
end MAT_FRAMEBUFFER ; 

architecture arch of MAT_FRAMEBUFFER is
    type symbol is array(0 to 2) of std_logic_vector(3 downto 0);
    type symbolGrid is array(0 to 8) of std_logic_vector(3 downto 0);

    constant fsymbols : symbol:=("0000","1001","1100");
    signal fsymbolGrid: symbolGrid:=(fsymbols(1),fsymbols(0),fsymbols(0),
                                    fsymbols(2), fsymbols(1), fsymbols(2),
                                    fsymbols(0), fsymbols(0), fsymbols(1));

    signal fline0 : std_logic_vector(7 downto 0) := line0;
    signal fline1 : std_logic_vector(7 downto 0) := line1;
    signal fline2 : std_logic_vector(7 downto 0) := line2;
    signal fline3 : std_logic_vector(7 downto 0) := line3;
    signal fline4 : std_logic_vector(7 downto 0) := line4;
    signal fline5 : std_logic_vector(7 downto 0) := line5;
    signal fline6 : std_logic_vector(7 downto 0) := line6;
    signal fline7 : std_logic_vector(7 downto 0) := line7;
    
    
    --top right, top middle, top left
    signal symbol_TR : std_logic_vector(3 downto 0):="0000";
    signal symbol_TM : std_logic_vector(3 downto 0):="0000";
    signal symbol_TL : std_logic_vector(3 downto 0):="0000";
    
    --middle right, middle, middle left
    signal symbol_MR : std_logic_vector(3 downto 0):="0000";
    signal symbol_MM : std_logic_vector(3 downto 0):="0000";
    signal symbol_ML : std_logic_vector(3 downto 0):="0000";
    --bottom left, bottom middle, bottom left
    signal symbol_BR : std_logic_vector(3 downto 0):="0000";
    signal symbol_BM : std_logic_vector(3 downto 0):="0000";
    signal symbol_BL : std_logic_vector(3 downto 0):="0000";
    


    constant div_constant : natural:=3000000;
    signal tmp  : std_logic;
    signal cnt  : natural range 0 to div_constant := 0;
    signal state: std_logic:='1';
    signal display_mode: std_logic:='0';
begin
    --fancy blink delay
    process( i_clock )
    begin
        if rising_Edge(i_clock) then
        cnt <= cnt+1;
        if (cnt=div_constant-1) then
            tmp <= NOT tmp;
            cnt <= 0;
        end if ;
        end if ;
    end process ; 

    --alternate symbols for fancy effects
    blinkSymbols : process( tmp )
    begin
        if rising_Edge(tmp) then
            case( state ) is
                when '1' =>
                    --top right
                    fline0(7 downto 6)<=symbol_TR(3 downto 2);
                    fline1(7 downto 6)<=symbol_TR(1 downto 0);   
                    --top middle
                    fline0(4 downto 3)<=symbol_TM(3 downto 2);
                    fline1(4 downto 3)<=symbol_TM(1 downto 0);  
                    --top left
                    fline0(1 downto 0)<=symbol_TL(3 downto 2);
                    fline1(1 downto 0)<=symbol_TL(1 downto 0); 
                    
                    --middle right
                    fline3(7 downto 6)<=symbol_MR(3 downto 2);
                    fline4(7 downto 6)<=symbol_MR(1 downto 0);   
                    --middle middle
                    fline3(4 downto 3)<=symbol_MM(3 downto 2);
                    fline4(4 downto 3)<=symbol_MM(1 downto 0);  
                    --middle left
                    fline3(1 downto 0)<=symbol_ML(3 downto 2);
                    fline4(1 downto 0)<=symbol_ML(1 downto 0); 

                    --bottom right
                    fline6(7 downto 6)<=symbol_BR(3 downto 2);
                    fline7(7 downto 6)<=symbol_BR(1 downto 0);   
                    --bottom middle
                    fline6(4 downto 3)<=symbol_BM(3 downto 2);
                    fline7(4 downto 3)<=symbol_BM(1 downto 0);  
                    --bottom left
                    fline6(1 downto 0)<=symbol_BL(3 downto 2);
                    fline7(1 downto 0)<=symbol_BL(1 downto 0); 

                    state<='0';
                when '0'=>
                    case( display_mode ) is
                    
                        when '0' =>
                        --top right
                        fline0(7 downto 6)<=symbol_TR(1 downto 0);  
                        fline1(7 downto 6)<=symbol_TR(3 downto 2);
                        --top middle
                        fline0(4 downto 3)<=symbol_TM(1 downto 0);
                        fline1(4 downto 3)<=symbol_TM(3 downto 2);  
                        --top left
                        fline0(1 downto 0)<=symbol_TL(1 downto 0);
                        fline1(1 downto 0)<=symbol_TL(3 downto 2); 
                        
                        --middle right
                        fline3(7 downto 6)<=symbol_MR(1 downto 0);
                        fline4(7 downto 6)<=symbol_MR(3 downto 2);   
                        --middle middle
                        fline3(4 downto 3)<=symbol_MM(1 downto 0);
                        fline4(4 downto 3)<=symbol_MM(3 downto 2);  
                        --middle left
                        fline3(1 downto 0)<=symbol_ML(1 downto 0);
                        fline4(1 downto 0)<=symbol_ML(3 downto 2); 
    
                        --bottom right
                        fline6(7 downto 6)<=symbol_BR(1 downto 0);
                        fline7(7 downto 6)<=symbol_BR(3 downto 2);   
                        --bottom middle
                        fline6(4 downto 3)<=symbol_BM(1 downto 0);
                        fline7(4 downto 3)<=symbol_BM(3 downto 2);  
                        --bottom left
                        fline6(1 downto 0)<=symbol_BL(1 downto 0);
                        fline7(1 downto 0)<=symbol_BL(3 downto 2); 
                            
                    
                    when others =>
                        --top right
                        fline0(7 downto 6)<="00";  
                        fline1(7 downto 6)<="00";
                        --top middle
                        fline0(4 downto 3)<="00";
                        fline1(4 downto 3)<="00";  
                        --top left
                        fline0(1 downto 0)<="00";
                        fline1(1 downto 0)<="00"; 
                        
                        --middle right
                        fline3(7 downto 6)<="00";
                        fline4(7 downto 6)<="00";   
                        --middle middle
                        fline3(4 downto 3)<="00";
                        fline4(4 downto 3)<="00";  
                        --middle left
                        fline3(1 downto 0)<="00";
                        fline4(1 downto 0)<="00"; 

                        --bottom right
                        fline6(7 downto 6)<="00";
                        fline7(7 downto 6)<="00";   
                        --bottom middle
                        fline6(4 downto 3)<="00";
                        fline7(4 downto 3)<="00";  
                        --bottom left
                        fline6(1 downto 0)<="00";
                        fline7(1 downto 0)<="00"; 
                    end case ;
                    state<='1';
                when others =>
            
            end case ;
        end if ;
    end process ; -- blinkSymbols
    

    --assing data to array
    fsymbolGrid(0)<=fsymbols(to_integer(unsigned(i_GridDat(17 downto 16))));
    fsymbolGrid(1)<=fsymbols(to_integer(unsigned(i_GridDat(15 downto 14))));
    fsymbolGrid(2)<=fsymbols(to_integer(unsigned(i_GridDat(13 downto 12))));
    fsymbolGrid(3)<=fsymbols(to_integer(unsigned(i_GridDat(11 downto 10))));
    fsymbolGrid(4)<=fsymbols(to_integer(unsigned(i_GridDat( 9 downto 8))));
    fsymbolGrid(5)<=fsymbols(to_integer(unsigned(i_GridDat( 7 downto 6))));
    fsymbolGrid(6)<=fsymbols(to_integer(unsigned(i_GridDat( 5 downto 4))));
    fsymbolGrid(7)<=fsymbols(to_integer(unsigned(i_GridDat( 3 downto 2))));
    fsymbolGrid(8)<=fsymbols(to_integer(unsigned(i_GridDat( 1 downto 0))));

    --assign array to helper signals
      --top right, top middle, top left
      symbol_TR <=fsymbolGrid(2);
      symbol_TM <=fsymbolGrid(1);
      symbol_TL <=fsymbolGrid(0);
      
      --middle right, middle, middle left
      symbol_MR <=fsymbolGrid(5);
      symbol_MM <=fsymbolGrid(4);
      symbol_ML <=fsymbolGrid(3);
      --bottom left, bottom middle, bottom left
      symbol_BR <=fsymbolGrid(8);
      symbol_BM <=fsymbolGrid(7);
      symbol_BL <=fsymbolGrid(6);

    

    o_frameData <= fline0 when i_selectLine = "000" else
        fline1 when i_selectLine = "001" else
        fline2 when i_selectLine = "010" else
        fline3 when i_selectLine = "011" else
        fline4 when i_selectLine = "100" else
        fline5 when i_selectLine = "101" else
        fline6 when i_selectLine = "110" else
        fline7 when i_selectLine = "111";
end architecture ;